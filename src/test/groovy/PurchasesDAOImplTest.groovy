package groovy

import com.jacobstuff.departmentstore.dao.interfaces.PurchasesDAO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import spock.lang.Specification

class PurchasesDAOImplTest extends Specification{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    def "should return the finalized ACO site hierarchy" () {

        given: "DAO Layer is called upon"
        PurchasesDAO purchasesDAO = Mock(PurchasesDAO)

        when:"finalizeAcoSiteHierarchy is invoked"
        purchasesDAO.getPurchasedItems()

        then: "will return the updated site hiearchy for ACO"
        1 * purchasesDAO.getPurchasedItems()
    }
}

package groovy

import com.jacobstuff.departmentstore.dao.interfaces.PromotionsDAO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import spock.lang.Specification

class PromotionsDAOImplTest extends Specification{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    def "should return the finalized ACO site hierarchy" () {

        given: "DAO Layer is called upon"
        PromotionsDAO promotionsDAO = Mock(PromotionsDAO)

        when:"finalizeAcoSiteHierarchy is invoked"
        promotionsDAO.getAllPromotions()

        then: "will return the updated site hiearchy for ACO"
        1 * promotionsDAO.getAllPromotions()
    }

}

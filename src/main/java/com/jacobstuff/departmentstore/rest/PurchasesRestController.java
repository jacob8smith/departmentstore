package com.jacobstuff.departmentstore.rest;

import com.jacobstuff.departmentstore.service.interfaces.PurchasesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class PurchasesRestController {

    @Autowired
    private PurchasesService purchases;

    @RequestMapping(value = "/all-purchased-items", method = RequestMethod.GET)
    public List<Map<String, Object>> getAllPurchases() {
        List<Map<String, Object>> result = null;
        try {
            result = purchases.getPurchasedItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (result);
    }
}

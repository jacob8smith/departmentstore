package com.jacobstuff.departmentstore.rest;

import com.jacobstuff.departmentstore.service.interfaces.PromotionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

public class PromotionsRestController {

    @Autowired
    private PromotionsService promotionSvc;

    @RequestMapping(value = "/all-promotions", method = RequestMethod.GET)
    public List<Map<String, Object>> getAllPromotions() {
        List<Map<String, Object>> result = null;
        try {
            result = promotionSvc.getAllPromotions();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (result);
    }
}

package com.jacobstuff.departmentstore.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestRestController {

    @RequestMapping(value = "/test")
    public String hello()
    {
        return "This is a RestController Test";
    }
}

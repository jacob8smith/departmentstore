package com.jacobstuff.departmentstore.dao.impl;

import com.jacobstuff.departmentstore.dao.interfaces.PurchasesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class PurchasesDAOImpl implements PurchasesDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getPurchasedItems() {
        return jdbcTemplate.queryForList("select PRODUCT_ID as \"productId\", CUSTOMER_ID as \"customerId\", QUANTITY as \"quantity\" from PURCHASES");
    }
}

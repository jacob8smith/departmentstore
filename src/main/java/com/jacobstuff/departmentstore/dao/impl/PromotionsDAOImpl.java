package com.jacobstuff.departmentstore.dao.impl;

import com.jacobstuff.departmentstore.dao.interfaces.PromotionsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

public class PromotionsDAOImpl implements PromotionsDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getAllPromotions() {
        return jdbcTemplate.queryForList("select PROMOTION_ID as \"promotionId\", NAME as \"name\", DURATION as \"duration\" from PROMOTIONS");
    }
}

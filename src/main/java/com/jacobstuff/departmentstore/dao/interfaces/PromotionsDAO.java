package com.jacobstuff.departmentstore.dao.interfaces;

import java.util.List;
import java.util.Map;

public interface PromotionsDAO {
    /**
     * @return
     */
    List<Map<String, Object>> getAllPromotions();
}

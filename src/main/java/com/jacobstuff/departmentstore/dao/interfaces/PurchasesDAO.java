package com.jacobstuff.departmentstore.dao.interfaces;

import java.util.List;
import java.util.Map;

public interface PurchasesDAO {
    /**
     * @return
     */
    List<Map<String, Object>> getPurchasedItems();
}

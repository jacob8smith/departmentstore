package com.jacobstuff.departmentstore.dto;

public class ProductTypesDTO {

    private int productTypeId;
    private String name;

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

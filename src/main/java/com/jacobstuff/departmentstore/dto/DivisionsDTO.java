package com.jacobstuff.departmentstore.dto;

public class DivisionsDTO {

    private char divisionID;
    private String name;

    public char getDivisionID() {
        return divisionID;
    }

    public void setDivisionID(char divisionID) {
        this.divisionID = divisionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

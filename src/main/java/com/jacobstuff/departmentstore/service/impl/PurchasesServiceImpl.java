package com.jacobstuff.departmentstore.service.impl;

import com.jacobstuff.departmentstore.dao.interfaces.PurchasesDAO;
import com.jacobstuff.departmentstore.service.interfaces.PurchasesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PurchasesServiceImpl implements PurchasesService {

    @Autowired
    private PurchasesDAO purchasesDAO;

    @Override
    public List<Map<String, Object>> getPurchasedItems() {
        return purchasesDAO.getPurchasedItems();
    }
}

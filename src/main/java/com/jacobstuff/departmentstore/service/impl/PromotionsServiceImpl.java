package com.jacobstuff.departmentstore.service.impl;

import com.jacobstuff.departmentstore.dao.interfaces.PromotionsDAO;
import com.jacobstuff.departmentstore.service.interfaces.PromotionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PromotionsServiceImpl implements PromotionsService {

    @Autowired
    private PromotionsDAO promotionsDAO;

    @Override
    public List<Map<String, Object>> getAllPromotions() {
        return promotionsDAO.getAllPromotions();
    }
}

package com.jacobstuff.departmentstore.service.interfaces;

import java.util.List;
import java.util.Map;

public interface PurchasesService {
    /**
     * @return
     */
    List<Map<String, Object>> getPurchasedItems();
}


package com.jacobstuff.departmentstore.service.interfaces;

import java.util.List;
import java.util.Map;

public interface PromotionsService {
    /**
     * @return
     */
    List<Map<String, Object>> getAllPromotions();
}

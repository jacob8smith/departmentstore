# Department Store Local Development

## Getting Started

These instructions will get you a copy of the Jacob Department Store application and set up your environment for local front-end and back-end (Spring Boot) development. 

### Prerequisites

Before we can build/run the application, we need to first do the following:

1. Clone the repo. 

2. Download and install NodeJs & NPM and install. 

3. Download and install OpenSSL if it isn't already on your machine.

4. Download and install Java SDK 1.8 (any minor version is fine).

5. Add the following line to `~/.bash_profile` to make sure your `JAVA_HOME` environment variable is set to the Java 8 installation:
<br>
```
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
```

6. Reload your bash profile by running `source ~/.bash_profile`.

7. Download and install Maven (only if you want to build from command line instead of IDE).

### Building/Running Back-end Spring Boot application

To run the Spring Boot application, I recommend using like IntelliJ because of their seamless Spring Boot integration. Simply add a new run configuration pointing to the main class `com.napa.pulse.PulseBootApplication`. Make sure the JRE used for this project is 1.8.

If you prefer to build and run from the command line instead, run the following commands from the root directory of the project:
<br>

```
mvn clean install
java -Dspring.profiles.active=<profile-name> -jar target/pulseui-*.jar
```


#### Spring Profiles

Regardless of how you choose to start the application (IDE or command line), you can configure at runtime what environment (i.e. dev, qa, prod, dr) and system (i.e. US or UAP) the application should point to by specifying a Spring Profile. This should be an option in the IDE's Spring Boot run configuration, or can be specified on the command line by adding the JVM arg `-Dspring.profiles.active=<profile-name>`.

The following profiles can be used:

* jac-dev (active by default)
* jac-qa
* jac-prod


The configuration files for these profiles can be found in `src/main/resources/application-<profile-name>.properties.`

Note: there are 3 levels of `application-XXX.properties` files, each being progressivly more specific to a given environment or system:

1. *Shared (i.e. `application.properties`)* - These are the properties that are shared by all profiles.

2. *Environment level (i.e. dev, qa, prod, dr)* - These properties are shared for a given database environment, regardless of the system (i.e. US or UAP). You do not need to explicitly select these profiles; they will be selected automatically based on which environment+system profile you choose.

When you start the application, the URL for the back-end API will be different based on the *system* chosen:

https://localhost:8443
